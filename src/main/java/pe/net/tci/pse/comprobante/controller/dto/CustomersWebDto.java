package pe.net.tci.pse.comprobante.controller.dto;


import pe.net.tci.pse.comprobante.core.domain.Customer;

import java.util.List;

public class CustomersWebDto {

    private List<Customer> customers;

    public CustomersWebDto(List<Customer> customers) {
        this.customers = customers;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }
}
