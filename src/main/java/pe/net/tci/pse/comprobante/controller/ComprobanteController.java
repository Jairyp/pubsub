package pe.net.tci.pse.comprobante.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.net.tci.pse.comprobante.controller.dto.CustomerWebDto;
import pe.net.tci.pse.comprobante.core.dto.CustomerDto;
import pe.net.tci.pse.comprobante.service.CustomerService;


@RestController
@RequestMapping("/comprobantes")
public class ComprobanteController {

    public static final String X_CORRELATION_ID_HEADER = "X-Correlation-Id";
    public static final String X_API_VERSION_HEADER = "X-Api-Version";
    public static final String X_API_FORCE_SYC_HEADER = "X-Api-Force-Sync";

    private CustomerService customerService;



    public ComprobanteController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping
    public HttpEntity<CustomerWebDto> create(@RequestHeader(name = X_API_FORCE_SYC_HEADER, required = false, defaultValue = "false") boolean forceSync,
                                             @RequestBody CustomerWebDto customerWebDto) {



        if (forceSync){
            customerService.save(customerWebDto.getCustomer());
            return ResponseEntity.ok().build();
        }else{
            CustomerDto customerDto = new CustomerDto();
            customerDto.setCustomer(customerWebDto.getCustomer());
            customerService.saveAsync(customerDto);
            return ResponseEntity.accepted()
                    .header(X_CORRELATION_ID_HEADER, customerDto.getCorrelation().getCorrelationId())
                    .build();
        }

    }

}
