package pe.net.tci.pse.comprobante.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.net.tci.pse.comprobante.core.domain.Customer;

import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private static final Logger log = LoggerFactory.getLogger(CustomerRepositoryImpl.class);

    private final JdbcTemplate jdbcTemplate;
    private final CustomerRowMapper customerRowMapper;




    public CustomerRepositoryImpl(JdbcTemplate jdbcTemplate,
                                  CustomerRowMapper customerRowMapper){
        this.jdbcTemplate=jdbcTemplate;
        this.customerRowMapper=customerRowMapper;

    }




    @Override
    public int save(Customer customer) {
        log.debug("save repository");


        return jdbcTemplate.update("call CustomersInsert(?,?,?);",new Object[] { customer.getNombre(),
                                                                             customer.getPaterno(),
                                                    customer.getPassword()});

    }

    @Override
    public void update(Customer customer) {
        log.info("update {}",customer.getCustomerId());

        jdbcTemplate.update("call CustomersUpdate(?,?,?,?);",new Object[] { customer.getCustomerId(),customer.getNombre(),
                customer.getPaterno(),
                customer.getPassword()});

    }

    @Override
    public void delete(Customer customer) {

        log.warn("delete{}",customer.getCustomerId());
        jdbcTemplate.update("call CustomersDelete(?);",new Object[] { customer.getCustomerId()});

    }





}
