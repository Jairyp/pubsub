package pe.net.tci.pse.comprobante.repository;

import pe.net.tci.pse.comprobante.core.domain.Customer;

public interface CustomerRepository {

    int save(Customer customer);
    void update(Customer customer) ;
    void delete(Customer customer);
}
