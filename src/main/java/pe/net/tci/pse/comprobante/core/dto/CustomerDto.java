package pe.net.tci.pse.comprobante.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import pe.net.tci.pse.comprobante.core.domain.Correlation;
import pe.net.tci.pse.comprobante.core.domain.Customer;
import pe.net.tci.pse.comprobante.core.domain.EventStatus;

import java.io.Serializable;


@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomerDto implements Serializable {

    private static final long serialVersionUID = 7193203766462383090L;

    private Correlation correlation;
    private Customer customer;
    private EventStatus status;
    private Exception eventError;

    public Correlation getCorrelation() {
        return correlation;
    }

    public void setCorrelation(Correlation correlation) {
        this.correlation = correlation;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public EventStatus getStatus() {
        return status;
    }

    public void setStatus(EventStatus status) {
        this.status = status;
    }

    public Exception getEventError() {
        return eventError;
    }

    public void setEventError(Exception eventError) {
        this.eventError = eventError;
    }
}
