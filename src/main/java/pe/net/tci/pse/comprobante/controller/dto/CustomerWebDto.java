package pe.net.tci.pse.comprobante.controller.dto;


import pe.net.tci.pse.comprobante.core.domain.Customer;

public class CustomerWebDto {

    private Customer customer;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
