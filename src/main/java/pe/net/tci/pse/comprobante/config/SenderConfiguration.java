package pe.net.tci.pse.comprobante.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.cloud.gcp.pubsub.integration.AckMode;
import org.springframework.cloud.gcp.pubsub.integration.inbound.PubSubInboundChannelAdapter;
import org.springframework.cloud.gcp.pubsub.integration.outbound.PubSubMessageHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.util.concurrent.ListenableFutureCallback;
import pe.net.tci.pse.comprobante.controller.dto.CustomerWebDto;
import pe.net.tci.pse.comprobante.core.domain.Customer;
import pe.net.tci.pse.comprobante.core.dto.CustomerDto;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;



@Configuration
public class SenderConfiguration {

    private static final Log logger = LogFactory.getLog(SenderConfiguration.class);

    private static final String TOPIC_NAME = "ExampleTopic";

    private static final String SUSCRIPTION_NAME = "exampleSubscription";

    @Bean
    public DirectChannel pubSubOutputChannel() {
        return new DirectChannel();
    }

    @Bean
    public DirectChannel pubSubInputChannel() {
        return new DirectChannel();
    }

    @Bean
    @ServiceActivator(inputChannel = "pubSubOutputChannel")
    public MessageHandler messageSender(PubSubTemplate pubSubTemplate) {
        PubSubMessageHandler adapter = new PubSubMessageHandler(pubSubTemplate, TOPIC_NAME);
        adapter.setPublishCallback(new ListenableFutureCallback<String>() {
            @Override
            public void onFailure(Throwable ex) {
                logger.info("Mensaje Con Error",ex);
            }

            @Override
            public void onSuccess(String result) {
                logger.info("Mensaje Enviado " + result);
            }
        });

        return adapter;
    }


    @Bean
    public PubSubInboundChannelAdapter messageChannelAdapter(
            @Qualifier("pubSubInputChannel") MessageChannel inputChannel,
            PubSubTemplate pubSubTemplate) {
        PubSubInboundChannelAdapter adapter =
                new PubSubInboundChannelAdapter(pubSubTemplate, SUSCRIPTION_NAME);
        adapter.setOutputChannel(inputChannel);
        adapter.setAckMode(AckMode.AUTO_ACK);
        adapter.setPayloadType(CustomerDto.class);

        return adapter;
    }

    @MessagingGateway(defaultRequestChannel = "pubSubOutputChannel")
    public interface PubSubCustomerGateway {
        void sendCustomerToPubSub(CustomerDto customer);
    }

}
