package pe.net.tci.pse.comprobante.service;


import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gcp.pubsub.support.BasicAcknowledgeablePubsubMessage;
import org.springframework.cloud.gcp.pubsub.support.GcpPubSubHeaders;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;
import pe.net.tci.pse.comprobante.config.SenderConfiguration.PubSubCustomerGateway;
import pe.net.tci.pse.comprobante.controller.dto.CustomerWebDto;
import pe.net.tci.pse.comprobante.core.domain.Application;
import pe.net.tci.pse.comprobante.core.domain.Correlation;
import pe.net.tci.pse.comprobante.core.domain.Customer;
import pe.net.tci.pse.comprobante.core.domain.EventStatus;
import pe.net.tci.pse.comprobante.core.dto.CustomerDto;
import pe.net.tci.pse.comprobante.repository.CustomerRepository;


@Service
public class CustomerServiceImplV2 implements CustomerService {

    private static final Logger log = LoggerFactory.getLogger(CustomerServiceImplV2.class);

    private CustomerRepository customerRepository;
    private PubSubCustomerGateway pubSubTemplate;

    public CustomerServiceImplV2(CustomerRepository customerRepository,
                                 PubSubCustomerGateway pubSubTemplate){
        this.customerRepository=customerRepository;
        this.pubSubTemplate=pubSubTemplate;
    }

    @Override
    public void saveAsync(CustomerDto customerDto) {

        Application application = new Application();
        application.setApplicationId("app01");

        Correlation correlation = new Correlation();
        correlation.setApplication(application);
        correlation.setCorrelationId(RandomStringUtils.randomAlphanumeric(40));
        customerDto.setCorrelation(correlation);
        customerDto.setStatus(EventStatus.PUTTED);

        pubSubTemplate.sendCustomerToPubSub(customerDto);
        /*rabbitTemplate.convertAndSend("clientes",
                "clientes.created.putted",
                customerDto);*/
    }




    /*@RabbitListener(queues = "clientes.crear")*/
    @ServiceActivator(inputChannel = "pubSubInputChannel")
    public void receiveMessage(final CustomerDto customerDto,
                               @Header(GcpPubSubHeaders.ORIGINAL_MESSAGE) BasicAcknowledgeablePubsubMessage message) {

        customerDto.setStatus(EventStatus.PROCESSING);

       /* rabbitTemplate.convertAndSend("clientes",
                "clientes.created.processing",
                customerDto);*/



        try{

            save(customerDto.getCustomer());

            customerDto.setStatus(EventStatus.SUCCESS);
            message.ack();
            /*rabbitTemplate.convertAndSend("clientes",
                    "clientes.created.success",
                    customerDto);*/

        }catch (Exception ex){
            customerDto.setStatus(EventStatus.FAILED);
            customerDto.setEventError(ex);
            message.ack();
           /* rabbitTemplate.convertAndSend("clientes",
                    "clientes.created.failed",
                    customerDto);*/

        }
    }


    @Override
    public void save(Customer customer) {
        log.info("save service");
        customer.setPassword(customer.getPassword());
        customerRepository.save(customer);
    }


}
