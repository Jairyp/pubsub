package pe.net.tci.pse.comprobante.core.domain;

public enum EventStatus {
    PUTTED,
    PROCESSING,
    SUCCESS,
    FAILED
}
