package pe.net.tci.pse.comprobante.service;

import pe.net.tci.pse.comprobante.core.domain.Customer;
import pe.net.tci.pse.comprobante.core.dto.CustomerDto;

public interface CustomerService {

    void saveAsync(CustomerDto customer);
    void save(Customer customer);


}
